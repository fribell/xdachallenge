package com.ribell.ferran.xposedchallenge;

import android.content.res.XModuleResources;
import android.os.Handler;
import android.os.Looper;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;

/**
 * Created by ferranribell on 28/10/2017.
 */

public class Tutorial implements IXposedHookZygoteInit, IXposedHookInitPackageResources {

    private static String MODULE_PATH = null;
    private Handler handler = new Handler(Looper.getMainLooper());
    private Runnable changeToRed = null;
    private Runnable changeToWhite = null;
    private Runnable blink = null;
    private long showBlinkTime = 60000;

    @Override
    public void initZygote(IXposedHookZygoteInit.StartupParam startupParam) throws Throwable {
        MODULE_PATH = startupParam.modulePath;
    }

    @Override
    public void handleInitPackageResources(final XC_InitPackageResources.InitPackageResourcesParam resparam) throws Throwable {
        if (!resparam.packageName.equals("com.android.systemui")) return;

        XposedBridge.log("we are system UI");
        changeToRed = new Runnable() {
            @Override
            public void run() {
                changeColorToRed(resparam);
            }
        };

        changeToWhite = new Runnable() {
            @Override
            public void run() {
                changeColorToWhite(resparam);
            }
        };

        blink = new Runnable() {
            @Override
            public void run() {
                blink();
            }
        };

        // That method is working but just change the assets and nothing else
        changeColorToRed(resparam);

        // That method is not working, I think that I have to find another way
        // to change the assets after the navbar is loaded
        //handler.postDelayed(blink, showBlinkTime);
    }

    private void changeColorToRed(XC_InitPackageResources.InitPackageResourcesParam resparam) {
        XModuleResources modRes = XModuleResources.createInstance(MODULE_PATH, resparam.res);
        try {
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_ime", modRes.fwd(R.drawable.n_back_down));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back", modRes.fwd(R.drawable.n_back));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_ime_land", modRes.fwd(R.drawable.n_back_down));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_land", modRes.fwd(R.drawable.n_back));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_side", modRes.fwd(R.drawable.n_back));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_home_land", modRes.fwd(R.drawable.n_home));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_recent", modRes.fwd(R.drawable.n_recents));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_recent_land", modRes.fwd(R.drawable.n_recents));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_recent_side", modRes.fwd(R.drawable.n_recents));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_home", modRes.fwd(R.drawable.n_home));
            } catch (Exception ignored) {
            }

        } catch (Exception e) {
            XposedBridge.log("Error hooking SystemUI resources\n" + e.getMessage());
        }

    }

    private void changeColorToWhite(XC_InitPackageResources.InitPackageResourcesParam resparam) {
        XModuleResources modRes = XModuleResources.createInstance(MODULE_PATH, resparam.res);
        try {
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_ime", modRes.fwd(R.drawable.w_back_down));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back", modRes.fwd(R.drawable.w_back));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_ime_land", modRes.fwd(R.drawable.w_back_down));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_land", modRes.fwd(R.drawable.w_back));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_back_side", modRes.fwd(R.drawable.w_back));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_home_land", modRes.fwd(R.drawable.w_home));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_recent", modRes.fwd(R.drawable.w_recents));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_recent_land", modRes.fwd(R.drawable.w_recents));
            } catch (Exception ignored) {
            }
            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_recent_side", modRes.fwd(R.drawable.w_recents));
            } catch (Exception ignored) {
            }

            try {
                resparam.res.setReplacement("com.android.systemui", "drawable", "ic_sysbar_home", modRes.fwd(R.drawable.w_home));
            } catch (Exception ignored) {
            }

        } catch (Exception e) {
            XposedBridge.log("Error hooking SystemUI resources\n" + e.getMessage());
        }
    }

    // Refactor that method to make it clever and configurable for the user
    private void blink() {
        handler.postDelayed(changeToRed, 500);
        handler.postDelayed(changeToWhite, 1000);
        handler.postDelayed(changeToRed, 1500);
        handler.postDelayed(changeToWhite, 2000);

        showBlinkTime -= 2500;
        if (showBlinkTime < 0) {
            handler.postDelayed(changeToRed, 500);
        } else {
            handler.postDelayed(blink, showBlinkTime);
        }
    }
}