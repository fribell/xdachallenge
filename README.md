# Droidcon UK droid2gether Hackathon 2017

## Workshop: Android and Xposed Framework
TO VIEW: https://gitpitch.com/xda/droid2gether-gitpitch

#### Idea
The idea is to make the user aware how many minutes is playing with the phone. So after some X minutes the navbar will blink from white to red, so the user is aware that the phone was switch on for X minutes. After other X minutes the phone will blink again but twice, so the user will know that he is looking the phone for X min*2. Like this until the user switch off the phone and do something more productive!

#### I've done...
- I've installed xpode framework in my old moto g 1st generation to test other moduls
- I could change the assests of the navbar
- I've created different assets to make it blink from red to white
- I couldn't add a timer to change it later, the phone wasn't booting after those changes

#### Issues?
- I think that I have to look to another aproach to set the timer
- Maybe replace the assets is not the right way, I should look for the imageview id and tint the assets instead.


It was a great weekend!I've learn new stuff that I didn't know. thanks